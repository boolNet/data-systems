// Created by Antwi Emmanuel  on 10/22/2019.
//

/*
 *
 * You will need to write your B+Tree almost entirely from scratch.
 *
 * B+Trees are dynamically balanced tree structures that provides efficient support for insertion, deletion, equality, and range searches.
 * The internal nodes of the tree direct the search and the leaf nodes hold the base data..
 *
 * For a basic rundown on B+Trees, we will refer to parts of Chapter 10 of the textbook Ramikrishnan-Gehrke
 * (all chapters and page numbers in this assignment prompt refer to the 3rd edition of the textbook).
 *
 * Read Chapter 10 which is on Tree Indexing in general. In particular, focus on Chapter 10.3 on B+Tree.
 */
#include <stdio.h>
#include <stdbool.h>
#ifdef _WIN32
#define false 0
#define true 1
#endif

#ifndef BTREE_H
#define BTREE_H

#include "data_types.h"
#include "query.h"


#include <stdlib.h>
#include <string.h>

/*
Designing your C Structs for B+Tree nodes (Chapter 10.3.1)
How will you represent a B+Tree as a C Struct (or series of C structs that work together)? There are many valid ways to do this part of your design, and we leave it open to you to try and tune this as you progress through the project.
How will you account for a B+Tree node being an internal node or a leaf node? Will you have a single node type that can conditionally be either of the two types, or will you have two distinct struct types?
How many children does each internal node have? This is called the fanout of the B+Tree.
What is the maximum size for a leaf node? How about for an internal node?
What is the minimum threshold of content for a node, before it has to be part of a rebalancing?
*/




// TODO: here you will need to define a B+Tree node(s) struct(s)
//Node is generic it can be either a leaf node or an internal node depending on the value
/*
 * Node has an array of pointers and another array for keys.
 * It contains a parent pointer that points to the parent
 * It has a boolean variable to tell whether node is either leaf or internal
 * It has a variables keysSize to check the number of keys in the node and a next pointer to point to
 * node on the right in the case of leaf node
 */

/*
 * This is a struct to hold the data temporarily on the memory when program is running.
 */
typedef struct data {
    int value;
} data;

/*
 * A struct to that represent the nodes in both leaf and internal part of the tree. It has a boolean that is set to
 * true when the node is a leaf and false when the node is an internal node.
 */
typedef struct node_object {
    int * keys;
    void ** pointers;
    struct node_object * parent;
    bool is_leaf;
    int keysSize;
} node;



/*
 * Gloabal variables that represents the default order or fan out for each node
 * and the root node also.
 */
int order = DEFAULT_ORDER;
node * root = NULL;


node * insert_into_parent(node * left, int key, node * right);

/*
 * A function to create a storage place for the values
 */
data * make_data(int value) {
    data * new_data = (data *)malloc(sizeof(data));
    new_data->value = value;
    return new_data;
}


/*
 * This function makes a node depending on whether it is a leaf node or an internal node
 */
node * createNode(void) {
    node * newNode;
    newNode = malloc(sizeof(node));
    newNode->pointers = malloc(order * sizeof(void *));
    newNode->keys = malloc((order - 1) * sizeof(int));
    newNode->parent = NULL;
    newNode->is_leaf = false;
    newNode->keysSize = 0;

    return newNode;
}

/*
 * Create a leaf node using the createNode function.
 */
node * makeLeafNode(void) {
    node * leaf = createNode();
    leaf->is_leaf = true;
    return leaf;
}


/* The following are methods that can be invoked on B+Tree node(s).
 * Hint: You may want to review different design patterns for passing structs into C functions.
 */

/* FIND (Chapter 10.4)
This is an equality search for an entry whose key matches the target key exactly.
How many nodes need to be accessed during an equality search for a key, within the B+Tree?
*/

// TODO: here you will need to define FIND/SEARCH related method(s) of finding key-values in your B+Tree.

/*
 * Function to get and return the leaf node in which the data is located
 */

node * getLeaf(int key) {
    if (root == NULL) {

        return root;
    }
    int j = 0;
    node * start_node = root;
    while (!start_node->is_leaf) {
        if (key> start_node->keys[start_node->keysSize-1])
            start_node = (node *)start_node->pointers[start_node->keysSize];
        else{
        j = 0;
        for (int k = 0; k < start_node->keysSize ; k++) {
            if (key >= start_node->keys[j]) j++;
            else break;
        }


        start_node = (node *)start_node->pointers[j];
    }}

    return start_node;
}

/* Finds and returns the data to which
 * a key refers.
 */
data * get(int key) {
    if (root == NULL) {
        return NULL;
    }
    node * leaf = NULL;
    leaf = getLeaf(key);
    int i = 0;
    for (i = 0; i < leaf->keysSize; i++) {
        if (leaf->keys[i] == key){
            break;
        }
    }
    if (i == leaf->keysSize) {
        return NULL;
    }else {
        return (data *) leaf->pointers[i];
    }
}
/* INSERT (Chapter 10.5)
How does inserting an entry into the tree differ from finding an entry in the tree?
When you insert a key-value pair into the tree, what happens if there is no space in the leaf node? What is the overflow handling algorithm?
For Splitting B+Tree Nodes (Chapter 10.8.3)
*/

// TODO: here you will need to define INSERT related method(s) of adding key-values in your B+Tree.
//Function to create new tree

/*
 * A helper function to help split the node into two when node is full and split needs to be done
 */
int split_data(int size) {
    if (size % 2 == 0) {
        return (size / 2);
    } else {
    return (size / 2 + 1);
}
}


/*
 * Function to split the leaf node and insert new key to parent node
 */
node * splitleafNode(node * leaf, int key, data * pointer) {
    int * temporaryKeys =  malloc(order * sizeof(int));
    void ** temporaryPointers =  malloc(order * sizeof(void *));

    int insertionIndex = 0;
    for (int i = 0; i < order-1 ; i++) {
        if (leaf->keys[insertionIndex] < key)insertionIndex++;
    }

    int m,k;
    for (m = 0, k = 0; m < leaf->keysSize; k++,m++) {
        if (k == insertionIndex) k++;
        temporaryKeys[k] = leaf->keys[m];
        temporaryPointers[k] = leaf->pointers[m];
    }

    temporaryKeys[insertionIndex] = key;
    temporaryPointers[insertionIndex] = pointer;

    leaf->keysSize = 0;
    int middleIndex = split_data(order - 1);

    for (m = 0; m < middleIndex; m++) {
        leaf->pointers[m] = temporaryPointers[m];
        leaf->keys[m] = temporaryKeys[m];
        leaf->keysSize++;
    }
    node * newLeaf = makeLeafNode();

    for (m = middleIndex, k = 0; m < order; m++, k++) {
        newLeaf->pointers[k] = temporaryPointers[m];
        newLeaf->keys[k] = temporaryKeys[m];
        newLeaf->keysSize++;
    }

    newLeaf->pointers[order - 1] = leaf->pointers[order - 1];
    leaf->pointers[order - 1] = newLeaf;

    for (m = leaf->keysSize; m < order - 1; m++)
        leaf->pointers[m] = NULL;
    for (m = newLeaf->keysSize; m < order - 1; m++)
        newLeaf->pointers[m] = NULL;

    newLeaf->parent = leaf->parent;
    int new_key = newLeaf->keys[0];
    free(temporaryPointers);
    free(temporaryKeys);
    return insert_into_parent(leaf, new_key, newLeaf);
}

/*
 * Insert into an internal node to after splitting
 */
node * insertToSplittedNode(node * old_node, int left_index,int key, node * right) {
    node ** temporaryPointers =  malloc((order + 1) * sizeof(node *));
    int * temporaryKeys =  malloc(order * sizeof(int));
    int i;
    int j;
    for (i = 0, j = 0; i < old_node->keysSize + 1; i++, j++) {
        if (j == left_index + 1) j++;
        temporaryPointers[j] = old_node->pointers[i];
    }

    for (i = 0, j = 0; i < old_node->keysSize; i++, j++) {
        if (j == left_index) j++;
        temporaryKeys[j] = old_node->keys[i];
    }

    temporaryPointers[left_index + 1] = right;
    temporaryKeys[left_index] = key;

    int middleIndex = split_data(order);
    node *newNode = createNode();
    old_node->keysSize = 0;
    for (i = 0; i < middleIndex - 1; i++) {
        old_node->pointers[i] = temporaryPointers[i];
        old_node->keys[i] = temporaryKeys[i];
        old_node->keysSize++;
    }
    old_node->pointers[i] = temporaryPointers[i];
    int righthalf = temporaryKeys[middleIndex - 1];
    for (++i, j = 0; i < order; i++, j++) {
        newNode->pointers[j] = temporaryPointers[i];
        newNode->keys[j] = temporaryKeys[i];
        newNode->keysSize++;
    }
    newNode->pointers[j] = temporaryPointers[i];
    free(temporaryPointers);
    free(temporaryKeys);
    newNode->parent = old_node->parent;
    node * child;
    for (i = 0; i <= newNode->keysSize; i++) {
        child = newNode->pointers[i];
        child->parent = newNode;
    }



    return insert_into_parent(old_node, righthalf, newNode);
}

/*
 * Insert into a parent node a new key from a splitted node below it
 */
node * insert_into_parent(node * left, int key, node * right) {
    node * parentNode = left->parent;

    if (parentNode == NULL){
        node * newRootNode = createNode();
        newRootNode->keys[0] = key;
        newRootNode->pointers[0] = left;
        newRootNode->pointers[1] = right;
        newRootNode->keysSize++;
        newRootNode->parent = NULL;
        left->parent = newRootNode;
        right->parent = newRootNode;
        return newRootNode;
    }

    int left_index = 0;
    for (int j = 0; j < parentNode->keysSize ; j++) {
        if (parentNode->pointers[left_index] != left)left_index++;
    }

    if (parentNode->keysSize < order - 1) {
        int i;

        for (i = parentNode->keysSize; i > left_index; i--) {
            parentNode->pointers[i + 1] = parentNode->pointers[i];
            parentNode->keys[i] = parentNode->keys[i - 1];
        }
        parentNode->pointers[left_index + 1] = right;
        parentNode->keys[left_index] = key;
        parentNode->keysSize++;
        return root;
    }

    return insertToSplittedNode(parentNode, left_index, key, right);
}

/*
 * Insert function which takes key and value as parameters and checks for four cases
 * depending on the case a decision is made
 * cases are
 * Case 1: Checks to see if root of is null and create a new root
 * case 2: Checks to see if key already exits replace the value with the new value
 * case 3: Check to see if there is a space in the node keys array in the node to allow insertion
 * Case 4: Split and insert if the node is full
 */
node * insert(int key, int value) {
    //Case 1
    data * newValue = NULL;
    if (root == NULL){
        newValue = make_data(value);
        node * root = makeLeafNode();
        root->keys[0] = key;
        root->pointers[0] = newValue;
        root->pointers[order - 1] = NULL;
        root->parent = NULL;
        root->keysSize++;
        return root;
    }

    //case 2
    newValue = get(key);
    if (newValue != NULL) {
        newValue->value = value;
        return root;
    }

    //case 3
    newValue = make_data(value);
    node * leaf = NULL;
    leaf = getLeaf(key);

    if (leaf->keysSize < order - 1) {
        int i;
        int insertPosition;
        insertPosition = 0;
        for (int j = 0; j < leaf->keysSize ; j++) {
            if (leaf->keys[insertPosition]<key)insertPosition++;
        }

        for (i = leaf->keysSize; i > insertPosition; i--) {
            leaf->keys[i] = leaf->keys[i - 1];
            leaf->pointers[i] = leaf->pointers[i - 1];
        }
        leaf->keys[insertPosition] = key;
        leaf->pointers[insertPosition] = newValue;
        leaf->keysSize++;
        return root;
    }

    //case 4

    return splitleafNode(leaf, key, newValue);
}


/* BULK LOAD (Chapter 10.8.2)
Bulk Load is a special operation to build a B+Tree from scratch, from the bottom up, when beginning with an already known dataset.
Why might you use Bulk Load instead of a series of inserts for populating a B+Tree? Compare the cost of a Bulk Load of N data entries versus that of an insertion of N data entries? What are the tradeoffs?
*/

// TODO: here you will need to define BULK LOAD related method(s) of initially adding all at once some key-values to your B+Tree.
// BULK LOAD only can happen at the start of a workload


/*RANGE (GRADUATE CREDIT)
Scans are range searches for entries whose keys fall between a low key and high key.
Consider how many nodes need to be accessed during a range search for keys, within the B+Tree?
Can you describe two different methods to return the qualifying keys for a range search?
(Hint: how does the algorithm of a range search compare to an equality search? What are their similarities, what is different?)
Can you describe a generic cost expression for Scan, measured in number of random accesses, with respect to the depth of the tree?
*/

// TODO GRADUATE: here you will need to define RANGE for finding qualifying keys and values that fall in a key range.



#endif//

